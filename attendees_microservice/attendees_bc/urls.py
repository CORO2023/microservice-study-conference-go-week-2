from django.contrib import admin
from django.urls import path, include   # ADD INCLUDE

urlpatterns = [
    path("admin/", admin.site.urls),

    # THIS IS A NEW LINE
    path("api/", include("attendees.api_urls")),
]
